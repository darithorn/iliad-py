import re
import string
from termcolor import colored

import parser

def tab_to_space (c, spaces=4):
    if c == "\t":
        return "".join (list (map (lambda x: " ", range (spaces))))
    return c

def escape (string):
    def _inner (s):
        if s == "\n":
            return "\\n"
        elif s == "\t":
            return "\\t"
        else:
            return s
    return "".join (map (_inner, string))

def ispunct (c):
    return string.punctuation.find (c) > 0

class Keyword ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "keyword"

class Identifier ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "identifier"

class Number ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "number"

class String ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "string"
    
class Character ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "character"

class Punctuation ():
    def __init__ (self, value, line):
        self.value = value
        self.line = line
        self.token_type = "punctuation"

# This seems terrible and stupid
# Do dictionaries have faster look-up times than plain arrays?
# The average for hash tables is O(1). An array is always O(1), direct indexing.
KEYWORDS = { "and": True,
             "begin": True,
             "do": True,
             "else": True,
             "end": True,
             "false": True,
             "for": True,
             "function": True,
             "if": True,
             "in": True,
             "nil": True,
             "not": True,
             "of": True,
             "or": True,
             "procedure": True,
             "program": True,
             "record": True,
             "then": True,
             "true": True,
             "type": True,
             "var": True,
             "while": True, }
              
N_TWO_OPS = { "==": True,
              "!=": True,
              ":=": True,
              "<=": True,
              ">=": True, }

class Lexer ():
    def __init__(self, file=None, char=None, col=None, line=None, current_line=None, toks=None, has_error=None, prev=None, defaults=False):
        if prev:
            self.file = prev.file
            self.char = prev.char
            self.col = prev.col
            self.line = prev.line
            self.current_line = prev.current_line
            self.toks = prev.toks
            self.has_error = prev.has_error
        if file != None: self.file = file
        if char != None: self.char = char
        if col != None: self.col = col
        if line != None: self.line = line
        if current_line != None: self.current_line = current_line
        if toks != None: self.toks = toks
        if has_error != None: self.has_error = has_error
        if defaults:
            # Still need to specify a file
            self.file = file
            self.current_line = file.readline ()
            self.char = self.current_line[0] if self.current_line != "" else ""
            self.col = 0
            self.line = 1
            self.toks = []
            self.has_error = False

def arrow (lexer, line=""):
    def _spaces (l="", c=0):
        if c < lexer.col - 1:
            return _spaces (l + " ", c + 1)
        return l
    def _shaft (l="", c=0):
        if c < len (lexer.current_line) - lexer.col - 1:
            return _shaft (l + "~", c + 1)
        return l
    return _shaft (_spaces () + "^")
        
def error (lexer, msg):
    line = colored (arrow (lexer), "red")
    print ("%s:%i:%i %s\n%s%s" % (colored ("error", "red"), lexer.line, lexer.col - 1, colored (msg, "white", attrs=["bold"]),
                                  lexer.current_line,
                                  line))
    return Lexer (has_error=True, prev=lexer)

def peek (lexer):
    return lexer.current_line[lexer.col + 1]

def getch (lexer):
    if lexer.current_line != "":
        if lexer.col + 1 >= len (lexer.current_line):
            line = lexer.file.readline ()
            return Lexer (current_line=line,
                          char=line[0] if line != "" else "",
                          col=0,
                          line=lexer.line + 1,
                          prev=lexer)
        else:
            return Lexer (char=lexer.current_line[lexer.col + 1],
                          col=lexer.col + 1,
                          prev=lexer)
    else:
        return Lexer (char="",
                      prev=lexer)

def lex_sl_comment (lexer):
    if lexer.char != "\n" and lexer.char != "":
        return lex_sl_comment (getch (lexer))
    else:
        return lexer
    
def lex_ml_comment (lexer):
    if lexer.char != "}" and lexer.char != "":
        return lex_ml_comment (getch (lexer))
    else:
        return lexer

def lex_operator (lexer):
    if ispunct (peek (lexer)):
        if N_TWO_OPS.get (lexer.char + peek (lexer)):
            return Lexer (toks=lexer.toks + [Punctuation (lexer.char + peek (lexer), lexer.line)],
                          prev=getch (lexer))
    return Lexer (toks=lexer.toks + [Punctuation (lexer.char, lexer.line)],
                  prev=lexer)
    
def lex_identifier (lexer, ident=""):
    if peek (lexer).isspace () or ispunct (peek (lexer)):
        if KEYWORDS.get (ident + lexer.char):
            return Lexer (toks=lexer.toks + [Keyword (ident + lexer.char, lexer.line)],
                          prev=lexer)
        return Lexer (toks=lexer.toks + [Identifier (ident + lexer.char, lexer.line)],
                      prev=lexer)
    elif lexer.char.isalpha ():
        return lex_identifier (getch (lexer), ident + lexer.char)
    else:
        return error (lexer, "Character is not valid for identifiers!")

def lex_number (lexer, number=""):
    if lexer.char.isnumeric ():
        return lex_number (getch (lexer), number + lexer.char)
    elif lexer.char == ".":
        if number.find (".") == -1:
            return lex_number (getch (lexer), number + lexer.char)
        else:
            return error (lexer, "The number already has a decimal!")
    elif lexer.char.isspace () or ispunct (lexer.char):
        return Lexer (toks=lexer.toks + [Number (number, lexer.line)],
                      prev=lexer)
    else:
        return error (lexer, "Unexpected character!")

def lex_string (lexer, s=""):
    if lexer.char != "\"" and lexer.char != "":
        return lex_string (getch (lexer), s + escape (lexer.char))
    elif lexer.char == "":
        return error (lexer, "Unexpected EOF. Expected closing quotation!")
    else:
        return Lexer (toks=lexer.toks + [String (s, lexer.line)],
                      prev=lexer)
    
def lexch (lexer):
    if lexer.char != "" and not lexer.has_error:
        if lexer.char == "/" and lexer.current_line[lexer.col + 1] == "/":
            return lexch (getch (lex_sl_comment (lexer)))
        elif lexer.char == "{":
            return lexch (getch (lex_ml_comment (lexer)))
        elif lexer.char == "\"":
            return lexch (getch (lex_string (getch (lexer))))
        elif ispunct (lexer.char):
            return lexch (getch (lex_operator (lexer)))
        elif lexer.char.isalpha ():
            return lexch (getch (lex_identifier (lexer)))
        elif lexer.char.isnumeric ():
            return lexch (lex_number (lexer))
        elif not lexer.char.isspace ():
            return lexch (getch (Lexer (toks=lexer.toks + [Character (lexer.char, lexer.line)],
                                        prev=lexer)))
        return lexch (getch (lexer))
    return lexer

lexer = Lexer (open ("test.il", "r"), defaults=True)
lexer = lexch (lexer)

my_parser = parser.Parser (toks=lexer.toks, defaults=True)
result = parser.parse (my_parser)
for a in result.ast:
    print (a.get ())

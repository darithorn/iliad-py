import re
from termcolor import colored

def escape (string):
    def _inner (s):
        if s == "\n":
            return "\\n"
        elif s == "\t":
            return "\\t"
        else:
            return s
    return "".join (map (_inner, string))

class Keyword ():
    def __init__ (self, name):
        self.name = name

    def get (self):
        return self.name

class Identifier ():
    def __init__ (self, ident):
        self.ident = ident

    def get (self):
        return self.ident

class Number ():
    def __init__ (self, value):
        self.value = value

    def get (self):
        return self.value

class Character ():
    def __init__ (self, c):
        self.c = c

    def get (self):
        return self.c

class Lexer ():
    def __init__ (self, file=None, char=None, current_line=None, line=None, col=None, has_error=None, toks=None, prev=None, defaults=False):
        if prev != None:
            self.file = prev.file
            self.current_line = prev.current_line
            self.char = prev.char
            self.line = prev.line
            self.col = prev.col
            self.has_error = prev.has_error
            self.char = prev.char
            self.toks = prev.toks
        if file: self.file = file
        if char: self.char = char
        if line: self.line = line
        if col: self.col = col
        if current_line:
            self.current_line = current_line
            if self.col >= len (self.current_line):
                self.char = ""
            else:
                self.char = self.current_line[self.col]
        if has_error: self.has_error = has_error
        if toks: self.toks = toks
        if defaults:
            self.current_line = self.file.readline ()
            self.line = 1
            self.col = 0
            self.char = self.current_line[self.col]
            self.has_error = False
            self.toks = []
        self.keywords = { "and": Keyword ("and"),
                          "begin": Keyword ("begin"),
                          "do": Keyword ("do"),
                          "else": Keyword ("else"),
                          "end": Keyword ("end"),
                          "false": Keyword ("false"),
                          "for": Keyword ("for"),
                          "function": Keyword ("function"),
                          "if": Keyword ("if"),
                          "in": Keyword ("in"),
                          "nil": Keyword ("nil"),
                          "not": Keyword ("not"),
                          "of": Keyword ("of"),
                          "or": Keyword ("or"),
                          "procedure": Keyword ("procedure"),
                          "record": Keyword ("record"),
                          "self": Keyword ("self"),
                          "string": Keyword ("string"),
                          "then": Keyword ("then"),
                          "true": Keyword ("true"),
                          "var": Keyword ("var"),
                          "while": Keyword ("while") }

def arrow (lexer, line=""):
    def _spaces (l="", c=0):
        if c < lexer.col - 1:
            return _spaces (l + " ", c + 1)
        return l
    def _shaft (l="", c=0):
        if c < len (lexer.current_line) - lexer.col:
            return _shaft (l + "~", c + 1)
        return l
    return _shaft (_spaces () + "^")
        
def error (lexer, msg):
    line = colored (arrow (lexer), "red")
    print ("%s:%i:%i %s\n%s%s" % (colored ("error", "red"), lexer.line, lexer.col - 1, colored (msg, "white", attrs=["bold"]), lexer.current_line, line))
    return Lexer (has_error=True, prev=lexer)
        
def getch (lexer):
    if lexer.current_line != "":
        if lexer.col + 1 >= len (lexer.current_line):
            return Lexer (current_line=lexer.file.readline (),
                          col=0,
                          line=lexer.line + 1,
                          prev=lexer)
        else:
            return Lexer (char=lexer.current_line[lexer.col],
                          col=lexer.col + 1,
                          prev=lexer)
    else:
        Lexer (char="",
               prev=lexer)

def peek (lexer):
    # TODO: Replace this with a method that doesn't mutate the lexer's `file`
    if lexer.col + 1 > len (lexer.current_line):
        pos = lexer.file.tell ()
        temp_line = lexer.file.readline ()
        char = lexer.current_line[lexer.col + 1]
        lexer.file.seek (pos)
        return char
    return lexer.current_line[lexer.col + 1]

# Lexes a single line comment
def lex_sl_comment (lexer):
    if lexer.char != "\n" or lexer.char != "":
        return lex_sl_comment (getch (lexer))
    else:
        return lexer

# Lexes a multi-line comment
def lex_ml_comment (lexer):
    if lexer.char != "}" or lexer.char != "":
        return lex_ml_comment (getch (lexer))
    else:
        return lexer

def lex_identifier (lexer, ident=""):
    if lexer.char.isalpha ():
        return lex_identifier (getch (lexer), ident + lexer.char)
    elif lexer.char.isspace ():
        # Python doesn't have a method that doesn't mutate the list :(
        if lexer.keywords.get (ident):
            lexer.toks.append (lexer.keywords[ident])
        else:
            lexer.toks.append (Identifier (ident))
        return lexer
    else:
        return error (lexer, "Character is not valid for identifiers!")
            
def lex_number (lexer, number=""):
    if lexer.char.isnumeric ():
        return lex_number (getch (lexer), number + lexer.char)
    elif lexer.char == ".":
        if number.find (".") == -1:
            return lex_number (getch (lexer), number + lexer.char)
        else:
            return error (lexer, "Unexpected .")
    elif lexer.char.isalpha ():
        if not number[-1].isalpha ():
            if re.match ("[fdlr]", lexer.char):
                return lex_number (getch (lexer), number + lexer.char)
            else:
                return error (lexer, "Invalid type specifier!")
        else:
            return error (lexer, "The number already has a type specifier!")
    elif lexer.char.isspace ():
        lexer.toks.append (Number (number))
        return lexer
    else:
        return error (lexer, "Unexpected value for number.")
        
    # Lexes a character
def lexch (lexer):
    if lexer.char != "" and not lexer.has_error:
        if lexer.char == "/":
            if peek (lexer) == "/":
                return lexch (getch (lex_sl_comment (lexer)))
        elif lexer.char == "{":
            return lexch (getch (lex_ml_comment (lexer)))
        elif lexer.char.isalpha ():
            l = lex_identifier (lexer)
            print (escape (l.char))
            return lexch (getch (l))
        elif lexer.char.isnumeric ():
            return lexch (getch (lex_number (lexer)))
        else:
            lexer.toks.append (Character (lexer.char))
            return lexch (getch (lexer))
    else:
        return lexer

def main ():
    #print (escape ("test\n"))
    lexer = Lexer (file=open ("test.il", "r"), defaults=True)
    lexer = lexch (lexer)
    for t in lexer.toks:
        print (t.get ())

if __name__ == "__main__":
    main ()

# iliad-py
A compiler in Python using the functional paradigm. The state of the current program is passed around functions and a new state is created and returned. 

`main.py` contains my first attempt at a lexer using the functional paradigm.
Look at `recursive.py` for the lexer, `parser.py` for the parser.

The lexer is in a working state. The parser isn't finished.

Why?
====
Because I can. With the rise of Haskell and other functional languages I've always wanted to learn the functional paradigm. This is an attempt to learn it while using a language I'm comfortable with. I set rules disallowing me from mutating any state and using loops, using recursive functions instead.

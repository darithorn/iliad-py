from termcolor import colored

class BeginBlock ():
    def __init__ (self):
        self.ast_type = "begin"

    def get (self):
        return "begin"

class EndBlock ():
    def __init__ (self):
        self.ast_type = "end"

    def get (self):
        return "end"
        
class NumberLiteral ():
    def __init__ (self, value):
        self.value = value
        self.ast_type = "number"

    def get (self):
        return self.value
        
class Procedure ():
    def __init__ (self, name, params):
        self.name = name
        self.params = params

    def get(self):
        return "procedure %s(%s)" % (self.name, "".join(map (lambda x: x.get() + ", ", self.params)))

class Var ():
    def __init__ (self, name, typename, value=False):
        self.name = name
        self.typename = typename
        self.value = value
        self.ast_type = "vardecl"

    def get (self):
        if self.typename == None:
            return self.name + " : auto"
        return self.name + " : " + self.typename

class VarEnd ():
    def __init__ (self, value):
        self.value = value
        self.ast_type = "vardecl_end"
    
class VarBlock ():
    def __init__ (self):
        self.ast_type = "varblock"

    def get (self):
        return "var"

class VarAssign ():
    def __init__ (self, name):
        self.name = name

    def get (self):
        return self.name + " :="

class Parameter ():
    def __init__ (self, name, typename):
        self.name = name
        self.typename = typename
        self.ast_type = "parameter"

    def get(self):
        return "%s: %s" % (self.name, self.typename)

class EmptyToken ():
    def __init__ (self):
        self.value = ""
        self.token_type = ""
        
class Parser ():
    def __init__ (self, toks=None, index=None, ast=None, has_error=None, prev=None, defaults=False):
        if prev:
            self.toks = prev.toks
            self.index = prev.index
            self.ast = prev.ast
            self.has_error = prev.has_error
        if toks != None: self.toks = toks
        if index != None: self.index = index
        if ast != None: self.ast = ast
        if has_error != None: self.has_error = has_error
        if defaults:
            # There's gotta be a better way than putting [None]
            self.toks = toks
            self.index = 0
            self.ast = []
            self.has_error = False
        if self.index < len (self.toks):
            self.cur = self.toks[self.index]
        else:
            self.cur = EmptyToken ()

def isempty (t):
    return t.value == "" and t.token_type == ""
            
def prev (parser):
    return parser.toks[0]
        
def next (parser):
    return Parser (index=parser.index + 1,
                   prev=parser)

def get_line (parser, line=None, string=""):
    if line == None:
        return get_line (next (parser), parser.cur.line, string + parser.cur.value + " ")
    elif parser.cur.line == line:
        return get_line (next (parser), line, string + parser.cur.value + " ")
    else:
        return string

def error (parser, msg):
    print ("%s:%s: %s" % (colored ("error", "red"), parser.cur.line, msg))
    print ("%s" % get_line (parser))
    return Parser (has_error=True,
                   prev=parser)

def parse_var_block (parser):
    if isempty (parser.cur):
        return error (parser, "Unexpected end of file! Expected `begin`")
    if parser.cur.token_type == "keyword":
        if parser.cur.value == "begin":
            # Different from `var` because `begin` has an `end` keyword
            return parse_begin_block (next (Parser (ast=parser.ast + [BeginBlock ()],
                                                    prev=parser)))
        else:
            return error (parser, "Unexpected %s. Expected `begin`" % parser.cur.value)
    if parser.cur.token_type != "identifier":
        return error (parser, "Expected identifier!")
    next1 = next(parser)
    if next1.cur.value == ":":
        next2 = next(next1)
        if next2.cur.token_type != "identifier":
            return error(next2, "Expected identifier!")
        next3 = next(next2)
        if next3.cur.value == ";":
            return parse_var_block(next(Parser(ast=parser.ast +
                                               [Var(parser.cur.value,
                                                    next2.cur.value)]),
                                        prev=next2))
        elif next3.cur.value == '=':
            next4 = Parser(ast=parser.ast + [Var(parser.cur.value,
                                                 next2.cur.value,
                                                 True)], prev=next(next3))
            next5 = parse_expression(next4)
            if next5.has_error:
                return next4
            if next5.cur.value != ";":
                return error(next4, "Expected ;")
            return parse_var_block(next(next5))
    if next1.cur.value != "=":
        return error(next1, "Expected =")
    next2 = Parser(ast=parser.ast + [Var(parser.cur.value,
                                         None,
                                         True)], prev=next(next1))
    next3 = parse_expression(next2)
    if next3.has_error:
        return next2
    if next3.cur.value != ";":
        return error(next2, "Expected ;")
    return parse_var_block(next(next3))
              

def parse_expression (parser):
    if parser.cur.token_type == "number":
        return Parser (ast=parser.ast + [NumberLiteral (parser.cur.value)],
                       prev=next (parser))
    else:
        return error(parser, "Expected number!")
     
def parse_var_assign (parser, ident):
    return parse_expression (Parser (ast=parser.ast + [VarAssign (ident)],
                                     prev=parser))

def parse_proc_call (parser, ident, args=[]):
    if isempty (parser.cur):
        return error (parser, "Unexpected end of file!")
    if parser.cur.value == ")":
        return Parser (ast=parser.ast + ProcCall (ident, args),
                       prev=parser)
    return parse_proc_call ()

def parse_statement (parser):
    # Could this be in a `let` in Haskell?
    def _let (parser):
        if isempty (parser.cur):
            return error (parser, "Unexpected end of file!")
        if parser.cur.token_type == "identifier":
            next1 = next (parser)
            if next1.cur.value == ":=":
                return parse_var_assign (next (next1), parser.cur.value)
            elif next1.cur.value == "(":
                return parse_proc_call (next (next1), parser.cur.value)
        else:
            return parser
    result = _let (parser)
    if result.cur.value != ";":
        return error (result, "Expected semi-colon. %s" % result.cur.value)
    else:
        return next (result)
        
def parse_begin_block (parser):
    if isempty (parser.cur):
        return error (parser, "Unexpected end of file!")
    if parser.cur.value == "end":
        return Parser (ast=parser.ast + [EndBlock ()],
                       prev=parser)
    return parse_begin_block (parse_statement (parser))

def parse_procedure_decl (parser):
    if parser.cur.token_type != "identifier":
        return error (parser, "Expected identifier for procedure name!")
    name = parser.cur.value
    next1 = next (parser)
    if next1.cur.value != "(":
        return error (parser, "Expected an open parenthesis! %s " % next1.cur)
    def _params (parser, params=[]):
        if parser.cur.value == ")":
            return (next (parser), params)
        if parser.cur.token_type != "identifier":
            return (error (parser, "Expected identifier for parameter name!"), [])
        next1 = next (parser)
        if next1.cur.value != ":":
            return (error (parser, "Expected colon!"), [])
        next2 = next (next1)
        if next2.cur.token_type != "identifier":
            return (error (parser, "Expected identifier for type name!"), [])
        next3 = next (next2)
        if next3.cur.value == ",":
            if next(next3).cur.value == ")":
                return error(next(next3), "Unexpected )")
            else:
                return _params(next(next3), params + [Parameter(parser.cur.value, next2.cur.value)])
        else:
            return _params (next3, params + [Parameter (parser.cur.value, next2.cur.value)])
    (result, params) = _params (next (next1))
    if result.has_error:
        return result
    result1 = Parser(ast=result.ast + [Procedure(name, params)],
                     prev=result)
    if result.cur.value == "var":
        return parse_var_block (next (Parser(ast=result1.ast +  [VarBlock()],
                                             prev=result1)))
    elif result.cur.value == "begin":
        return parse_begin_block (next (Parser (ast=result1.ast + [BeginBlock ()],
                                                prev=result1)))
    else:
        return error (result, "Expected `begin` for procedure body!")

def parse (parser):
    if not isempty (parser.cur) and not parser.has_error:
        if parser.cur.token_type == "keyword":
            if parser.cur.value == "procedure":
                return parse (parse_procedure_decl (next (parser)))
            if parser.cur.value == "begin":
                return error (parser, "Unexpected `begin`!")
            if parser.cur.value == "var":
                return error (parser, "Unexpected `var`!")
        return parse (next (parser))
    else:
        return parser
